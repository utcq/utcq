<div align="center">
  <img src="https://github-readme-stats.vercel.app/api?username=utcq&hide_title=false&hide_rank=false&show_icons=true&include_all_commits=true&count_private=true&disable_animations=false&theme=dracula&locale=en&hide_border=false&order=1" height="150" alt="stats graph"  />
  <img height="150" src="https://yt3.ggpht.com/a-/AAuE7mBb6gYUH3m-6VOwD64IqdZ3DbGERtxc8V-zdg=s900-mo-c-c0xffffffff-rj-k-no"  />
</div>

###

<br clear="both">

<div align="center">
  <img src="https://skillicons.dev/icons?i=py" height="40" alt="python logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=c" height="40" alt="c logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=rust" height="40" alt="rust logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=linux" height="40" alt="linux logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=bash" height="40" alt="bash logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=git" height="40" alt="git logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=gitlab" height="40" alt="gitlab logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=neovim" height="40" alt="neovim logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=vscode" height="40" alt="vscode logo"  />
</div>

###
